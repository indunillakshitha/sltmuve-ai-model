import pickle
from sklearn.linear_model import LinearRegression

import joblib

print('Training Started ....')

data=pickle.load(open('data.pickle','rb'))
target_linear=pickle.load(open('target_count.pickle','rb'))


clsfr_linear=LinearRegression()
clsfr_linear.fit(data,target_linear)
joblib.dump(clsfr_linear,'Rides_predictor_linear_count.sav')

print('Training Completed ...100%')
