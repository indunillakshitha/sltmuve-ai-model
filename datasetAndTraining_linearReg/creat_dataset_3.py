import pandas as pd

# pickle use to save features and labels as pickle format
import pickle
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

csv_new = pd.read_csv("my_csv_with_count_cleared.csv").values
print('creating pickle.....')
data = csv_new[:, 0:3]
# target=csv_new[:,4]
target = csv_new[:, 3]

train_data, test_data, train_target, test_target = train_test_split(data, target, test_size=0.1)

from sklearn.linear_model import LinearRegression

clsfr = LinearRegression()
clsfr.fit(train_data, train_target)
results = clsfr.predict(test_data)
# accuaracy=accuracy_score(test_target,results)
# print("Accuaracy : ",accuaracy)


from sklearn.metrics import r2_score

r2_value = r2_score(test_target, results)

print('*************************************')
print('*************************************')
print('r2_score:', r2_value)
print('*************************************')
print('*************************************')
# print('Actual value:', test_target[0:10])
# print('*************************************')
# print('*************************************')
#
# print('Predicted value', results[0:10])
# print('*************************************')
# print('*************************************')

pickle.dump(data, open('data.pickle', 'wb'))
# pickle.dump(target,open('target.pickle','wb'))
pickle.dump(target, open('target_count.pickle', 'wb'))
print('pickle file creating ---- Done')
